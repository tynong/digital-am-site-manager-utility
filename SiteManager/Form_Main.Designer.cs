﻿namespace SiteManager
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.tabControl_Sites = new System.Windows.Forms.TabControl();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl_ActionControlList = new System.Windows.Forms.TabControl();
            this.tabPage_Mgmt = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel_ActionControlList = new System.Windows.Forms.TableLayoutPanel();
            this.actionControl_SSH = new SiteManager.Controls.ActionControl_SSH();
            this.actionControl_SFTP1 = new SiteManager.Controls.ActionControl_SFTP();
            this.actionControl_RDP1 = new SiteManager.Controls.ActionControl_RDP();
            this.tabPage_LogViewer = new System.Windows.Forms.TabPage();
            this.tabPage_Automation = new System.Windows.Forms.TabPage();
            this.actionControl_Information = new SiteManager.Controls.ActionControl_Information();
            this.imageList_Icons = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel.SuspendLayout();
            this.tabControl_ActionControlList.SuspendLayout();
            this.tabPage_Mgmt.SuspendLayout();
            this.tableLayoutPanel_ActionControlList.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl_Sites
            // 
            this.tabControl_Sites.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_Sites.Location = new System.Drawing.Point(4, 4);
            this.tabControl_Sites.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl_Sites.Name = "tabControl_Sites";
            this.tableLayoutPanel.SetRowSpan(this.tabControl_Sites, 2);
            this.tabControl_Sites.SelectedIndex = 0;
            this.tabControl_Sites.Size = new System.Drawing.Size(315, 802);
            this.tabControl_Sites.TabIndex = 0;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel.Controls.Add(this.tabControl_Sites, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.tabControl_ActionControlList, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.actionControl_Information, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(924, 810);
            this.tableLayoutPanel.TabIndex = 2;
            // 
            // tabControl_ActionControlList
            // 
            this.tabControl_ActionControlList.Controls.Add(this.tabPage_Mgmt);
            this.tabControl_ActionControlList.Controls.Add(this.tabPage_LogViewer);
            this.tabControl_ActionControlList.Controls.Add(this.tabPage_Automation);
            this.tabControl_ActionControlList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_ActionControlList.Location = new System.Drawing.Point(326, 141);
            this.tabControl_ActionControlList.Name = "tabControl_ActionControlList";
            this.tabControl_ActionControlList.SelectedIndex = 0;
            this.tabControl_ActionControlList.Size = new System.Drawing.Size(595, 666);
            this.tabControl_ActionControlList.TabIndex = 1;
            // 
            // tabPage_Mgmt
            // 
            this.tabPage_Mgmt.Controls.Add(this.tableLayoutPanel_ActionControlList);
            this.tabPage_Mgmt.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Mgmt.Name = "tabPage_Mgmt";
            this.tabPage_Mgmt.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Mgmt.Size = new System.Drawing.Size(587, 637);
            this.tabPage_Mgmt.TabIndex = 0;
            this.tabPage_Mgmt.Text = "Management Interface";
            this.tabPage_Mgmt.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel_ActionControlList
            // 
            this.tableLayoutPanel_ActionControlList.ColumnCount = 1;
            this.tableLayoutPanel_ActionControlList.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel_ActionControlList.Controls.Add(this.actionControl_SSH, 0, 0);
            this.tableLayoutPanel_ActionControlList.Controls.Add(this.actionControl_SFTP1, 0, 1);
            this.tableLayoutPanel_ActionControlList.Controls.Add(this.actionControl_RDP1, 0, 2);
            this.tableLayoutPanel_ActionControlList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_ActionControlList.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel_ActionControlList.Name = "tableLayoutPanel_ActionControlList";
            this.tableLayoutPanel_ActionControlList.RowCount = 4;
            this.tableLayoutPanel_ActionControlList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_ActionControlList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_ActionControlList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_ActionControlList.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_ActionControlList.Size = new System.Drawing.Size(581, 631);
            this.tableLayoutPanel_ActionControlList.TabIndex = 0;
            // 
            // actionControl_SSH
            // 
            this.actionControl_SSH.BackColor = System.Drawing.Color.LightSteelBlue;
            this.actionControl_SSH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionControl_SSH.Location = new System.Drawing.Point(3, 3);
            this.actionControl_SSH.Name = "actionControl_SSH";
            this.actionControl_SSH.Size = new System.Drawing.Size(575, 151);
            this.actionControl_SSH.TabIndex = 0;
            // 
            // actionControl_SFTP1
            // 
            this.actionControl_SFTP1.BackColor = System.Drawing.Color.Bisque;
            this.actionControl_SFTP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionControl_SFTP1.Location = new System.Drawing.Point(3, 160);
            this.actionControl_SFTP1.Name = "actionControl_SFTP1";
            this.actionControl_SFTP1.Size = new System.Drawing.Size(575, 151);
            this.actionControl_SFTP1.TabIndex = 1;
            // 
            // actionControl_RDP1
            // 
            this.actionControl_RDP1.BackColor = System.Drawing.Color.LemonChiffon;
            this.actionControl_RDP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionControl_RDP1.Location = new System.Drawing.Point(3, 317);
            this.actionControl_RDP1.Name = "actionControl_RDP1";
            this.actionControl_RDP1.Size = new System.Drawing.Size(575, 151);
            this.actionControl_RDP1.TabIndex = 2;
            // 
            // tabPage_LogViewer
            // 
            this.tabPage_LogViewer.Location = new System.Drawing.Point(4, 25);
            this.tabPage_LogViewer.Name = "tabPage_LogViewer";
            this.tabPage_LogViewer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_LogViewer.Size = new System.Drawing.Size(587, 637);
            this.tabPage_LogViewer.TabIndex = 1;
            this.tabPage_LogViewer.Text = "Log Viewer";
            this.tabPage_LogViewer.UseVisualStyleBackColor = true;
            // 
            // tabPage_Automation
            // 
            this.tabPage_Automation.Location = new System.Drawing.Point(4, 25);
            this.tabPage_Automation.Name = "tabPage_Automation";
            this.tabPage_Automation.Size = new System.Drawing.Size(587, 637);
            this.tabPage_Automation.TabIndex = 2;
            this.tabPage_Automation.Text = "Automation";
            this.tabPage_Automation.UseVisualStyleBackColor = true;
            // 
            // actionControl_Information
            // 
            this.actionControl_Information.BackColor = System.Drawing.Color.PapayaWhip;
            this.actionControl_Information.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionControl_Information.Location = new System.Drawing.Point(326, 3);
            this.actionControl_Information.Name = "actionControl_Information";
            this.actionControl_Information.Size = new System.Drawing.Size(595, 132);
            this.actionControl_Information.TabIndex = 2;
            // 
            // imageList_Icons
            // 
            this.imageList_Icons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Icons.ImageStream")));
            this.imageList_Icons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Icons.Images.SetKeyName(0, "folderClose");
            this.imageList_Icons.Images.SetKeyName(1, "folderOpen");
            this.imageList_Icons.Images.SetKeyName(2, "linux");
            this.imageList_Icons.Images.SetKeyName(3, "windows");
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 810);
            this.Controls.Add(this.tableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form_Main";
            this.Text = "Site Manager";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tabControl_ActionControlList.ResumeLayout(false);
            this.tabPage_Mgmt.ResumeLayout(false);
            this.tableLayoutPanel_ActionControlList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_Sites;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.ImageList imageList_Icons;
        private System.Windows.Forms.TabControl tabControl_ActionControlList;
        private System.Windows.Forms.TabPage tabPage_Mgmt;
        private System.Windows.Forms.TabPage tabPage_LogViewer;
        private Controls.ActionControl_Information actionControl_Information;
        private System.Windows.Forms.TabPage tabPage_Automation;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_ActionControlList;
        private Controls.ActionControl_SSH actionControl_SSH;
        private Controls.ActionControl_SFTP actionControl_SFTP1;
        private Controls.ActionControl_RDP actionControl_RDP1;
    }
}

