﻿namespace SiteManager.Controls
{
    partial class ActionControl_SFTP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_LaunchWinScp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_LaunchWinScp
            // 
            this.button_LaunchWinScp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_LaunchWinScp.Location = new System.Drawing.Point(410, 5);
            this.button_LaunchWinScp.Name = "button_LaunchWinScp";
            this.button_LaunchWinScp.Size = new System.Drawing.Size(139, 28);
            this.button_LaunchWinScp.TabIndex = 0;
            this.button_LaunchWinScp.Text = "Launch WinSCP";
            this.button_LaunchWinScp.UseVisualStyleBackColor = true;
            // 
            // ActionControl_SFTP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.Controls.Add(this.button_LaunchWinScp);
            this.Name = "ActionControl_SFTP";
            this.Size = new System.Drawing.Size(555, 140);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_LaunchWinScp;
    }
}
