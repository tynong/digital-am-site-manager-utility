﻿namespace SiteManager.Controls
{
    partial class ActionControl_Information
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_SelectedItemDesc = new System.Windows.Forms.Label();
            this.label_SelectedItemComment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_SelectedItemDesc
            // 
            this.label_SelectedItemDesc.AutoSize = true;
            this.label_SelectedItemDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SelectedItemDesc.Location = new System.Drawing.Point(24, 20);
            this.label_SelectedItemDesc.Name = "label_SelectedItemDesc";
            this.label_SelectedItemDesc.Size = new System.Drawing.Size(274, 25);
            this.label_SelectedItemDesc.TabIndex = 0;
            this.label_SelectedItemDesc.Text = "description of selected item";
            // 
            // label_SelectedItemComment
            // 
            this.label_SelectedItemComment.AutoSize = true;
            this.label_SelectedItemComment.Location = new System.Drawing.Point(39, 89);
            this.label_SelectedItemComment.Name = "label_SelectedItemComment";
            this.label_SelectedItemComment.Size = new System.Drawing.Size(72, 17);
            this.label_SelectedItemComment.TabIndex = 1;
            this.label_SelectedItemComment.Text = "comments";
            // 
            // ActionControl_Information
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PapayaWhip;
            this.Controls.Add(this.label_SelectedItemComment);
            this.Controls.Add(this.label_SelectedItemDesc);
            this.Name = "ActionControl_Information";
            this.Size = new System.Drawing.Size(567, 265);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_SelectedItemDesc;
        private System.Windows.Forms.Label label_SelectedItemComment;
    }
}
