﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace SiteManager.Controls
{
    public partial class ActionControl_SSH : ActionControl_Generic
    {
        public ActionControl_SSH()
        {
            InitializeComponent();
        }

        protected override void ItemDetailsChangedAction()
        {
            HostDetails h = _itemDetails.NodeDetails as HostDetails;
            if (h != null)
            {
                string domain = h.site.domain;
                string arguments = " -ssh -l " + SessionInfo.userId + " " + h.hostName + "a." + domain;
                this.textBox_puttyCmdLine.Text = arguments;
            }
        }

        private void button_LauchPutty_Click(object sender, EventArgs e)
        {
            string strPuttyExe = SessionInfo.puttyExe;
            string arguments = this.textBox_puttyCmdLine.Text;
            ProcessStartInfo startInfo = new ProcessStartInfo(strPuttyExe, arguments);

            startInfo.UseShellExecute = false;

            Process p = new Process();
            p.StartInfo = startInfo;
            p.Start();
        }
    }
}
