﻿namespace SiteManager.Controls
{
    partial class ActionControl_RDP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_LaunchRdp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_LaunchRdp
            // 
            this.button_LaunchRdp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_LaunchRdp.Location = new System.Drawing.Point(368, 7);
            this.button_LaunchRdp.Name = "button_LaunchRdp";
            this.button_LaunchRdp.Size = new System.Drawing.Size(140, 27);
            this.button_LaunchRdp.TabIndex = 0;
            this.button_LaunchRdp.Text = "Remote Desktop";
            this.button_LaunchRdp.UseVisualStyleBackColor = true;
            // 
            // ActionControl_RDP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.button_LaunchRdp);
            this.Name = "ActionControl_RDP";
            this.Size = new System.Drawing.Size(515, 126);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_LaunchRdp;
    }
}
