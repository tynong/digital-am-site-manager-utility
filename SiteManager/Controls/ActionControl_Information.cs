﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SiteManager.Controls
{
    public partial class ActionControl_Information : ActionControl_Generic
    {
        public ActionControl_Information()
        {
            InitializeComponent();
        }

      
        protected override void ItemDetailsChangedAction()
        {
            this.label_SelectedItemDesc.Text = _itemDetails.NodeDetails.desc;
            this.label_SelectedItemComment.Text = _itemDetails.NodeDetails.comments;

            switch (_itemDetails.nodeType)
            {
                case NodeType.context: break;
                case NodeType.group: break;
                case NodeType.host: break;
                default: break;
            }
            
        }

        
        
    }
}
