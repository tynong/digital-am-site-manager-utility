﻿namespace SiteManager.Controls
{
    partial class ActionControl_SSH
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_LauchPutty = new System.Windows.Forms.Button();
            this.label_putty_cmd = new System.Windows.Forms.Label();
            this.textBox_puttyCmdLine = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button_LauchPutty
            // 
            this.button_LauchPutty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_LauchPutty.Location = new System.Drawing.Point(336, 5);
            this.button_LauchPutty.Name = "button_LauchPutty";
            this.button_LauchPutty.Size = new System.Drawing.Size(131, 26);
            this.button_LauchPutty.TabIndex = 0;
            this.button_LauchPutty.Text = "Launch PuTTY";
            this.button_LauchPutty.UseVisualStyleBackColor = true;
            this.button_LauchPutty.Click += new System.EventHandler(this.button_LauchPutty_Click);
            // 
            // label_putty_cmd
            // 
            this.label_putty_cmd.AutoSize = true;
            this.label_putty_cmd.Location = new System.Drawing.Point(20, 10);
            this.label_putty_cmd.Name = "label_putty_cmd";
            this.label_putty_cmd.Size = new System.Drawing.Size(73, 17);
            this.label_putty_cmd.TabIndex = 1;
            this.label_putty_cmd.Text = "command:";
            // 
            // textBox_puttyCmdLine
            // 
            this.textBox_puttyCmdLine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_puttyCmdLine.Location = new System.Drawing.Point(15, 37);
            this.textBox_puttyCmdLine.Multiline = true;
            this.textBox_puttyCmdLine.Name = "textBox_puttyCmdLine";
            this.textBox_puttyCmdLine.Size = new System.Drawing.Size(452, 86);
            this.textBox_puttyCmdLine.TabIndex = 2;
            // 
            // ActionControl_SSH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Controls.Add(this.textBox_puttyCmdLine);
            this.Controls.Add(this.label_putty_cmd);
            this.Controls.Add(this.button_LauchPutty);
            this.Name = "ActionControl_SSH";
            this.Size = new System.Drawing.Size(478, 135);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_LauchPutty;
        private System.Windows.Forms.Label label_putty_cmd;
        private System.Windows.Forms.TextBox textBox_puttyCmdLine;
    }
}
