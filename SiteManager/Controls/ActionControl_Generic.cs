﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SiteManager.Controls
{
    public partial class ActionControl_Generic : UserControl
    {

        public ActionControl_Generic()
        {
            InitializeComponent();
        }

        protected SelectedItemDetails _itemDetails;

        public SelectedItemDetails itemDetails
        {
            set
            {
                this._itemDetails = value;
                ItemDetailsChangedAction();
            }
        }

        protected virtual void ItemDetailsChangedAction()
        {
        }
    }
}
