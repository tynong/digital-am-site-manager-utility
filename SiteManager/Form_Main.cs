﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SiteManager
{
    internal partial class Form_Main : Form
    {
        // my lazy way of passing ancestor information to leaf nodes
        // TODO: fix this laziness
        private SiteDetails processing_site;
        private ContextDetails processing_context;
        private GroupDetails processing_group;

        public Form_Main(string[] args)
        {
            InitializeComponent();

            XmlDocument dom = new XmlDocument();

            try
            {
                string configPath = @"C:\zues\config\config_min.xml";
                if (args.Length > 0)
                {
                    configPath = args[0];
                    Debug.Write("****** configPath ****: " + configPath);
                }
                dom.Load(configPath);
                XmlNodeList nodeList = dom.DocumentElement.ChildNodes;

                for (int i = 0; i < nodeList.Count; i++)
                {
                    XmlNode xn = nodeList[i];

                    switch (xn.Name)
                    {
                        case "site_list": PopulateSites(xn);    break;

                        case "user_setting": LoadUserSetting(xn);
                            Debug.WriteLine("user_setting"); break;

                        case "interface_list":
                            Debug.WriteLine("interface_list"); break;

                        case "action_list":
                            Debug.WriteLine("action_list"); break;
                    }
                }
            }
            catch (XmlException e)
            {
                System.Console.WriteLine(e.Message);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void LoadUserSetting(XmlNode xmlNode_UserSetting)
        {
            XmlNodeList nodeList = xmlNode_UserSetting.ChildNodes;

            for (int i = 0; i < nodeList.Count; i++)
            {
                XmlNode xn = nodeList[i];

                if (xn.Name == "admin_id")
                {
                    SessionInfo.userId = xn.InnerText;
                }
                else if (xn.Name == "bin_path")
                {
                    SessionInfo.binPath = xn.InnerText;
                    SessionInfo.puttyExe = SessionInfo.binPath + "\\putty.exe";
                }
            }
        }
        private void PopulateSites(XmlNode xmlNode_siteList)
        {
            XmlNodeList nodeList = xmlNode_siteList.ChildNodes;

            for (int i = 0; i < nodeList.Count; i++)
            {
                XmlNode xn = nodeList[i];

                SiteDetails site = new SiteDetails(xn, DefaultBackColor);

                this.processing_site = site;

                TabPage tp = new TabPage(site.desc);

                this.tabControl_Sites.TabPages.Add(tp);

                this.PopulateTab(tp, xn.ChildNodes, site);
            }
            
        }

        private void PopulateTab(TabPage tab, XmlNodeList xNodeList, SiteDetails site)
        {
            TreeView tv = new TreeView();
            tv.Dock = DockStyle.Fill;
            tv.BackColor = site.bgColor;
            tv.ImageList = this.imageList_Icons;

            tv.AfterExpand += tv_AfterExpand;
            tv.AfterCollapse += tv_AfterCollapse;

            tv.AfterSelect += tv_AfterSelect;

            tab.Controls.Add(tv);

            for (int i = 0; i < xNodeList.Count; i++)
            {
                if (xNodeList[i].Name == "context")
                {
                    ContextDetails context = new ContextDetails(xNodeList[i]);

                    this.processing_context = context;

                    TreeNode tNode = new TreeNode(context.desc);

                    tNode.Tag = new TreeNodeTag(NodeType.context, context);

                    tv.Nodes.Add(tNode);

                    if (xNodeList[i].HasChildNodes) 
                    {
                        this.PopulateGroup(xNodeList[i].ChildNodes, tNode);
                    }
                    if (context.expend == "all")
                    {
                        tNode.ExpandAll();
                    }
                }
            }
        }

        private void PopulateGroup(XmlNodeList xNodeList, TreeNode tNode)
        {
            for (int i = 0; i < xNodeList.Count; i++)
            {
                XmlNode xNode = xNodeList[i];

                if (xNode.Name == "group")
                {
                    GroupDetails group = new GroupDetails(xNode);

                    this.processing_group = group;
                    
                    TreeNode tn = new TreeNode(group.groupName);
                    tn.Tag = new TreeNodeTag(NodeType.group, group);
                    tNode.Nodes.Add(tn);
                    if (xNode.HasChildNodes)
                    {
                        this.PopulateGroup(xNode.ChildNodes, tn);
                    }
                }
                else if (xNode.Name == "host")
                {
                    HostDetails host = new HostDetails(xNode);

                    host.site = this.processing_site;

                    TreeNode tn = new TreeNode(host.hostName);

                    tn.Tag = new TreeNodeTag(NodeType.host, host);
                    tn.ImageKey = host.os;
                    tn.SelectedImageKey = host.os;

                    tNode.Nodes.Add(tn);
                }
            }
        }

        void tv_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNodeTag t = e.Node.Tag as TreeNodeTag;

            if (t != null)
            {
                SelectedItemDetails item = new SelectedItemDetails(t.NotdeType, t.NodeDetails);
                this.actionControl_Information.itemDetails = item;
                this.actionControl_SSH.itemDetails = item;
            }
        }

        void tv_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            e.Node.ImageKey = "folderClose";
            e.Node.SelectedImageKey = "folderClose";
        }

        void tv_AfterExpand(object sender, TreeViewEventArgs e)
        {
            e.Node.ImageKey = "folderOpen";
            e.Node.SelectedImageKey = "folderOpen";
        }


        //private void AddNode(XmlNode inXmlNode, TreeNode inTreeNode)
        //{
        //    if (inXmlNode.Name == "host")
        //    {
        //        HostDetails host = new HostDetails(inXmlNode);

        //        inTreeNode.Text = host.hostName;
        //        inTreeNode.Name = host.hostName;

        //        inTreeNode.ImageKey = host.os;
        //        inTreeNode.SelectedImageKey = host.os;

        //        TreeNodeTag t = new TreeNodeTag(TreeNodeTag.NodeTypes.host);
        //        t.NodeDetails = host;

        //        inTreeNode.Tag = t;
        //    }
        //    else if (inXmlNode.HasChildNodes)
        //    {
        //        XmlNodeList childNodes = inXmlNode.ChildNodes;

        //        for (int i = 0; i <= childNodes.Count - 1; i++)
        //        {
        //            XmlNode xNode = childNodes[i];

        //            if (xNode.Name == "group")
        //            {
        //                string nodeName = HelperUtil.GetXmlNodeAttributeValue(xNode, "desc", "NoNameGroup");
        //                TreeNode tn = new TreeNode(nodeName);
        //                inTreeNode.Nodes.Add(tn);
        //            }

        //            AddNode(xNode, inTreeNode.Nodes[i]);
        //        }
        //    }
             
        //    else
        //    {
        //        //throw new Exception("invalid node: " + inXmlNode.OuterXml);
        //    }
        //}


        /*
         private void AddNode(XmlNode inXmlNode, TreeNode inTreeNode)
        {
            if (inXmlNode.Name == "group")
            {
                if (inXmlNode.HasChildNodes)
                {
                    XmlNodeList nodeList = inXmlNode.ChildNodes;
                    for (int i = 0; i <= nodeList.Count - 1; i++)
                    {
                        XmlNode xNode = inXmlNode.ChildNodes[i];
                        string nodeName = HelperUtil.GetXmlNodeAttributeValue(xNode, "desc", "NoNameGroup");
                        TreeNode treeNode_Group = new TreeNode(nodeName);
                        inTreeNode.Nodes.Add(treeNode_Group);
                        
                        AddNode(xNode, inTreeNode.Nodes[i]);
                    }
                }
            }
            else if (inXmlNode.Name == "host")
            {
                HostDetails host = new HostDetails(inXmlNode);

                inTreeNode.Text = host.hostName;
                inTreeNode.Name = host.hostName;

                inTreeNode.ImageKey = host.os;
                inTreeNode.SelectedImageKey = host.os;

                TreeNodeTag t = new TreeNodeTag(TreeNodeTag.NodeTypes.host);
                t.NodeDetails = host;

                inTreeNode.Tag = t;
            }
            else
            {
                Debug.WriteLine("unknown node");
            }
        }
         
         
         */

    }
}


