﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Drawing;

namespace SiteManager
{
    public enum NodeType { host, group, context };

    public static class SessionInfo
    {
        public static string userId;
        public static string binPath;
        public static string puttyExe;
    }

    public class TreeNodeTag
    {

        public NodeType NotdeType;
        public ItemDetails NodeDetails;

        public TreeNodeTag(NodeType n, ItemDetails d)
        {
            this.NotdeType = n;
            this.NodeDetails = d;
        }
    }

    public class SelectedItemDetails
    {
        public NodeType nodeType;
        public ItemDetails NodeDetails;

        public SelectedItemDetails(NodeType n, ItemDetails d)
        {
            this.nodeType = n;
            this.NodeDetails = d;
        }
    }

    public class ItemDetails
    {
        public string desc;
        public string comments;
    }

    class SiteDetails : ItemDetails
    {
        const string defaultSite = "NO_DESC";
        public string siteName;
        public string domain;
        public string defaultInterface;
        public Color bgColor;

        public SiteDetails(XmlNode n, Color defaultBackColor)
        {
            this.siteName = HelperUtil.GetXmlNodeAttributeValue(n, "id", defaultSite);
            this.desc = HelperUtil.GetXmlNodeAttributeValue(n, "desc", this.siteName);
            this.comments = HelperUtil.GetXmlNodeAttributeValue(n, "comments");
            this.domain = HelperUtil.GetXmlNodeAttributeValue(n, "domain"); // default empty
            this.defaultInterface = HelperUtil.GetXmlNodeAttributeValue(n, "default_interface");
            this.bgColor = HelperUtil.GetColorFromXmlNodeAttributeValue(n, "bgcolor", defaultBackColor);
        }
    }

    class ContextDetails : ItemDetails
    {
        public string expend;

        public ContextDetails(XmlNode n)
        {
            this.desc = HelperUtil.GetXmlNodeAttributeValue(n, "desc");
            this.comments = HelperUtil.GetXmlNodeAttributeValue(n, "comments");
            this.expend = HelperUtil.GetXmlNodeAttributeValue(n, "expend");
        }
    }

    class GroupDetails : ItemDetails
    {
        const string defaultGroup = "NO group id";
        public string groupName;

        public GroupDetails parentGroup;

        public GroupDetails(XmlNode n)
        {
            this.groupName = HelperUtil.GetXmlNodeAttributeValue(n, "id", defaultGroup);
            this.desc = HelperUtil.GetXmlNodeAttributeValue(n, "desc", this.groupName);
            this.comments = HelperUtil.GetXmlNodeAttributeValue(n, "comments");
        }
    }
    class HostDetails : ItemDetails
    {
        const string defaultHost = "NO host id";
        const string defaultOS = "linux";
        public string hostName;
        public string os;
        public string dc;

        public GroupDetails group;
        public SiteDetails site;

        public HostDetails(XmlNode n)
        {
            this.hostName = HelperUtil.GetXmlNodeAttributeValue(n, "id", defaultHost);
            this.os = HelperUtil.GetXmlNodeAttributeValue(n, "os", defaultOS);
            this.desc = HelperUtil.GetXmlNodeAttributeValue(n, "desc", this.hostName);
            this.comments = HelperUtil.GetXmlNodeAttributeValue(n, "comments");
            this.dc = HelperUtil.GetXmlNodeAttributeValue(n, "dc");
        }
    }


}
