﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Diagnostics;

namespace SiteManager
{
    internal class HelperUtil
    {
        static internal string GetXmlNodeAttributeValue(XmlNode node, string attribute)
        {
            return GetXmlNodeAttributeValue(node, attribute, string.Empty);
        }

        static internal string GetXmlNodeAttributeValue(XmlNode node, string attribute, string defaultValue)
        {
            if (node.Attributes[attribute] != null)
                return node.Attributes[attribute].Value;
            else
                return defaultValue;
        }

        static internal Color GetColorFromXmlNodeAttributeValue(XmlNode node, string colorAttr, Color defaultColor)
        {
            if (IsAttrValueNullOrEmpty(node.Attributes[colorAttr]))
                return defaultColor;
            else
            {
                Color l;
                try
                {
                    string attrVal = node.Attributes[colorAttr].Value;
                    l = Color.FromName(attrVal);
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                    l = defaultColor;
                }

                return l;
            }
        }

        static internal bool IsAttrValueNullOrEmpty(XmlAttribute attr)
        {
            return (attr == null || attr.Value  == string.Empty);
        }
    }
}
